# Component based MVC framework

This light MVC framework is designed to encourage groupping PHP code into small components with their own MVC structure

### Features

- Models
- Views
- Controllers
- Component structure
- Router class
- Light database wrapper
- Custom templating engine
- Dynamic css and js inclusion

### Usage

Here are some examples of the functionality:

Adding routes:
```php
$router = new Router();

// Controller:action
$router->get('/', 'HomePage:index'); 
$router->get('/user/{userId}', 'User:getUser');
$router->post('/user/add', 'User:addUser');
$router->add('GET', '/pages/latest', 'Page:getLatestPages');

```

Fetching data from the database and loading a view:

```php
class PageController extends Controller {

    public function getLatestPages($params) {

        $pages = DB::query("SELECT * FROM pages ORDER BY id DESC LIMIT 10");
        
        // Component/View
        View::load('Page/PageList', [
            'Pages' => $pages,
        ]);

    }

```

Rendering data in the template

```html
<!-- including the component css file -->
{css PageList}

{if $Pages}
    <section class="pagelist">
        {each $Pages as $page}
            <section class="pagelist__page">
                <h1 class="pagelist__page-title">$page['title']</h1>
                <h2 class="pagelist__page-subtitle">$page['subtitle']</h2>
                <div class="pagelist__page-contents">
                    $page['content']
                </div>
                {if $page['link']}
                    <a class="pagelist__page-link" href="$page['link']">Open page</a>
                {/if}
            </section>
        {/each}
    </section>
{/if}

```