<?php

    class HomePageController extends Controller {
        
        public function index($params) {
            
            $users = DB::query("SELECT * FROM users ORDER BY id DESC");
            
            View::load('HomePage', [
                'CurrentLink' => '/',
                'Skills1' => [
                    'HTML5',
                    'CSS3/Less',
                    'Javascript ES5+',
                    'ReactJS',
                    'jQuery',
                    'PHP',
                ],
                'Skills2' => [
                    'NodeJS',
                    'MySQL, Postgres',
                    'Git, SVN',
                    'NginX',
                    'Debian Linux',
                ]
            ]);
        }
    
        public function add() {

            $result = DB::query(
                "INSERT INTO pages (title, url_segment) values(:title, :url_segment)",
                ['title' => 'page 1', 'url_segment' => 'url-1']
            );


            if (!empty($params)) {
                print_r($params);
            }
            
            View::load('User.User', [
                'name' => 'hi',
                'title' => 'oh hi',
            ]);
        }

        public function delete() {
            $db = new DB();
            $result = $db->query(
                "DELETE FROM users WHERE name = :name",
                ['name' => 'Dude']
            );
            View::load('index', [
                'name' => 'hi',
                'title' => 'oh hi',
                'result' => $result,
            ]);
        }
    }