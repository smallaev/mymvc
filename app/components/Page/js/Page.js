
let fadeInElements = document.querySelectorAll('.words-fade-in');

fadeInElements.forEach((fadeInEl, idx) => {
    let html = fadeInEl.innerHTML;
    let words = html.split(' ');
    let htmlWords = words.map((el, idx) => {
        if (el.length) {
            return `<span>${el}</span>`;
        } else {
            return '';
        }
    });

    fadeInEl.innerHTML = htmlWords.join(' ');
    fadeInEl.classList.add('visible');
    let wordElements = document.querySelectorAll('.words-fade-in span');
    setTimeout(() => {
        let delay = 0;
        wordElements.forEach((el, idx) => {
            el.setAttribute('style', `transition-delay: ${delay}ms`);
            el.classList.add('visible');
            delay += 10;
        })
    }, 10);
});
