<?php

    class PageController extends Controller {
        
        public function index($params) {
            $data = DB::query("SELECT * FROM pages ORDER BY id DESC");
            
            View::load('Page', [
                'PageData' => $data,
            ]);
        }
    
        public function add() {
            // $result = DB::query(
            //     "INSERT INTO pages (title, url_segment) values(:title, :url_segment)",
            //     ['title' => 'page 1', 'url_segment' => 'url-1']
            // );
        }
    }