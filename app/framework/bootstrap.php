<?php

	// bootstrap file - loading the config and autoloading all requested classes

	// load the config
	require_once 'config/main.php';

	spl_autoload_register(function($class) {
		// autoload Core classes
		if (file_exists(__DIR__ . '/core/' . $class . '.php')) {
			require_once __DIR__ . '/core/' . $class . '.php';
		}

		// autoload Model classes
		if (file_exists(__DIR__ . '/components/' . $class . '/' . $class . '.php')) {
			require_once __DIR__ . '/components/' . $class . '/' . $class . '.php';
		}
	});