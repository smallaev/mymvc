<?php

	// DB Params
	define('DB_HOST', '127.0.0.1');
	define('DB_USER', 'root');
	define('DB_PASSWORD', 'root');
	define('DB_NAME', 'mvc');
	define('DB_PORT', '3306');
	define('DB_CHARSET', 'utf8mb4');

	// Site
	define('BASE_URL', '[your site base URL]');