<?php

  /*
   * Base Template class
   * Compiles custom templates into PHP templates
   * 
   */
  class Template {

    public function __construct() {
    }

    public function __destruct() {
    }

    /**
     * Parses custom template and compiles to PHP template
     * @param string $view - Name of the view
     * @param string @template - The custom template
     * @param array $data - custom data passed to the view from the controller
     *   
     */
    public static function parse($view, $template, $data = []) {

        // extracts the data array so that it's available by the PHP tmplate after it's compiled and required below
        extract($data, EXTR_SKIP);
        
        $template = self::parseExtendBlocks($template);
        $template = self::parseIncludeBlocks($template);
        $template = self::parseEchoBlocks($template);
        $template = self::parseIfBlocks($template);
        $template = self::parseEachBlocks($template);
        $template = self::parseCssBlocks($template);
        $template = self::parseJsBlocks($template);

        // make sure the directory with compiled PHP templates exists and is accessible
        if (!is_dir(__DIR__ . '/../compiled')) {
            if (!mkdir(__DIR__ . '/../compiled')) {
                throw new Exception("Can't make a new fodler for compiled templates");
            }
        }

        $view = str_replace('/', '.', $view);
        
        $templateFile = __DIR__ . '/../compiled/'. $view . '.php';

        // compiles the parsed template file into a PHP template and requires the file here
        file_put_contents($templateFile, $template);
        require $templateFile;
    }

    /**
     * Parses {if ...} blocks
     * @param string $template
     * @return string
     */
    public static function parseIfBlocks($template) {
        $template = preg_replace('/\{if (.*?)\}/s', '<?php if ($1): ?>', $template);
        $template = preg_replace('/\{else\}/s', '<?php else: ?>', $template);
        $template = preg_replace('/\{\/if\}/s', '<?php endif; ?>', $template);
        return $template;
    }

    /**
     * Parses {$ ...} blocks
     * @param string $template
     * @return string
     */
    public static function parseEchoBlocks($template) {
        $template = preg_replace('/\{(\$.*?)\}/s', '<?php echo $1 ?>', $template);
        return $template;
    }

    /**
     * Parses {each ...} blocks
     * @param string $template
     * @return string
     */
    public static function parseEachBlocks($template) {
        $template = preg_replace('/\{each (.*?) as (.*?)\}/s', '<?php foreach ($1 as $2): ?>', $template);
        $template = preg_replace('/\{\/each\}/s', '<?php endforeach; ?>', $template);
        return $template;
    }

    /**
     * Parses {extend ...} blocks
     * @param string $template
     * @return string 
     */
    public static function parseExtendBlocks($template) {
        preg_match('/\{extend (?P<extendFile>[\\w.]+)\}/s', $template, $matches);
        if (!empty($matches['extendFile'])) {

            // removes the {extend ...} line from the template
            $newFile = str_replace($matches[0], '', $template);

            // checks if the extend template exists
            $extendFileName = $matches['extendFile'];
            $extendFilePathComponents = __DIR__ . '/../../components/'. $extendFileName . '/views/' . $extendFileName . '.html';
            if (file_exists($extendFilePathComponents)) {
                // getting the contents of the extend file and replacing the {@extend} placeholder with the contents of the current template file 
                $extendFileContent = file_get_contents($extendFilePathComponents);
                $template = str_replace('{@extend}', $newFile, $extendFileContent);
            } else {
                throw new Exception("View {$extendFileName} doesn't exist");
                
            }
        }

        return $template;
    }

    /**
     * Parses {include ...} blocks
     * @param string $template
     * @return string 
     */
    public static function parseIncludeBlocks($template) {
        preg_match_all('/\{include (?P<includeFile>[\\w.\/]+)\}/s', $template, $matches);
        // if include blocks exist in the template
        if (!empty($matches['includeFile'])) {
            $i = 0;

            // loop over all include block matches
            foreach($matches['includeFile'] as $includeFile) {
                if (strpos($includeFile, '/') !== false) {
                    // if the include path contains /, use the full path
                    $filePath = $includeFile;
                } else {
                    // otherwise locate the file in the views folder
                    $filePath = $includeFile . '/views/' . $includeFile;
                }
                $includeFilePathComponents = __DIR__ . '/../../components/'. $filePath . '.html';
                if (file_exists($includeFilePathComponents)) {
                    // if include file exists, get contents and replace the include block in the current template with the contents of the file
                    $includeFileContent = file_get_contents($includeFilePathComponents);
                    $template = str_replace($matches[0][$i], $includeFileContent, $template);
                }
                $i++;
            }            
        }

        return $template;
    }

    /**
     * Parses {css ...} blocks
     * @param string $template
     * @return string 
     */
    public static function parseCssBlocks($template) {

        // checks if the template contains the /*{@css}*/ css placeholder
        preg_match('/\/\*\{@css\}\*\//s', $template, $placeholderMatches);

        if (!empty($placeholderMatches[0])) {
            // checks all {css ...} blocks in the template
            preg_match_all('/\{css (?P<cssFile>[\\w.\/]+)\}/s', $template, $matches);
            if (!empty($matches['cssFile'])) {
                $i = 0;
                // loops over all {css ...} block matches
                foreach($matches['cssFile'] as $cssFile) {
                    if (strpos($cssFile, '/') !== false) {
                        $filePath = $cssFile;
                    } else {
                        $filePath = $cssFile . '/css/' . $cssFile;
                    }
                    $cssFilePathComponents = __DIR__ . '/../../components/'. $filePath . '.css';
                    // if the css file exists, get the contents and replace the placeholder with the contents of the css file
                    if (file_exists($cssFilePathComponents)) {
                        $cssFileContent = file_get_contents($cssFilePathComponents);
                        $template = str_replace($placeholderMatches[0], $cssFileContent.'/*{@css}*/', $template);
                        $template = str_replace($matches[0][$i], '', $template);
                    }
                    $i++;
                }            
            }
        }

        return $template;
    }

    /**
     * Parses {js ...} blocks
     * @param string $template
     * @return string 
     */
    public static function parseJsBlocks($template) {

        // checks if the template contains the /*{@js}*/ css placeholder
        preg_match('/\/\/\{@js\}/s', $template, $placeholderMatches);

        if (!empty($placeholderMatches[0])) {
            preg_match_all('/\{js (?P<jsFile>[\\w.\/]+)\}/s', $template, $matches);

            if (!empty($matches['jsFile'])) {
                $i = 0;

                // loops over all {js ...} block matches
                foreach($matches['jsFile'] as $jsFile) {
                    if (strpos($jsFile, '/') !== false) {
                        $filePath = $jsFile;
                    } else {
                        $filePath = $jsFile . '/js/' . $jsFile;
                    }
                    $jsFilePathComponents = __DIR__ . '/../../components/'. $filePath . '.js';
                    // if the js file exists, get the contents and replace the placeholder with the contents of the js file
                    if (file_exists($jsFilePathComponents)) {
                        $jsFileContent = file_get_contents($jsFilePathComponents);
                        $jsFileContent = '(function(){' . $jsFileContent . '}());';
                        $template = str_replace($placeholderMatches[0], $jsFileContent.'//{@js}', $template);
                        $template = str_replace($matches[0][$i], '', $template);
                    }
                    $i++;
                }            
            }
        }

        return $template;
    }


    
  }