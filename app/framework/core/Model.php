<?php

    /**
     * Model class
     * Contains basic DB methods to get database records
     */
    
    class Model {

        /**
         * Retrieves rows for the model calling the method
         * @param string $where The WHERE part of the SQL query
         * @param string $order The ORDER BY part of the query
         * @param string $limit The LIMIT part of the query
         * @param string $offset The OFFSET part of the query
         * @param bool $getCount The flag that determines if the method returnes the count instead of rows
         * @return mixed Array of query results or the row count
         */
        public static function get($where = null, $order = null, $limit = null, $offset = null, $getCount = false) {

            // we get the caller class, ie Page::get(...)
            $className = get_called_class();
            if (class_exists($className)) {
                // we check if the caller class has the table name property defined
                if (property_exists($className, 'tableName')) {
                    $tableName = $className::$tableName;
                    // if $getCount is true, we only return the count result of the query
                    if ($getCount) {
                        $sqlQuery = 'SELECT COUNT(*) AS count FROM ' . $tableName;
                    } else {
                        $sqlQuery = 'SELECT * FROM ' . $tableName;
                    }
                    if ($where) {
                        $sqlQuery .= (' WHERE ' . $where);
                    }
                    if ($order) {
                        $sqlQuery .= (' ORDER BY ' . $order);
                    }
                    if ($limit) {
                        $sqlQuery .= (' LIMIT ' . $limit);
                    }
                    if ($offset) {
                        $sqlQuery .= (' OFFSET ' . $offset);
                    }

                    // delegate to the DB class once the query is built
                    $result = DB::query($sqlQuery);
                    return $result;
                } else {
                    throw new Exception("The class {$className} doesn't have a tableName property");
                }
            } else {
                return null;
            }
        }

        /**
         * Gets one record from the DB
         * @param string $where The WHERE part of the SQL query
         * @param string $order The ORDER BY part of the query
         * @param string $offset The OFFSET part of the query
         * @return mixed Array of query results or the row count
         */
        public static function getOne($where = null, $order = null, $offset = null) {
            return self::get($where, $order, 1, $offset);
        }

        /**
         * Gets a random record from the DB
         * @param string $where The WHERE part of the SQL query
         * @param string $limit The LIMIT part of the query
         * @param string $offset The OFFSET part of the query
         * @return mixed Array of query results or the row count
         */
        public static function getRandom($where = null, $limit = 1, $offset = null) {
            return self::get($where, 'RAND()', $limit, $offset);
        }

        /**
         * Gets a row count for a particular query
         * @param string $where The WHERE part of the SQL query
         * @param string $order The ORDER BY part of the query
         * @param string $limit The LIMIT part of the query
         * @param string $offset The OFFSET part of the query
         * @return mixed Array of query results or the row count
         */
        public static function getCount($where = null, $order = null, $limit = null, $offset = null) {
            $result = self::get($where, $order, $limit, $offset, true);
            if (!empty($result) && isset($result[0]['count'])) {
                return (int)$result[0]['count'];
            }
        }
    }