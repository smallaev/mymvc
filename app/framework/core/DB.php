<?php

  /**
   * Database wrapper class
   * Handles database connection using PDO
   */
  class DB {

	private $host = DB_HOST;
	private $user = DB_USER;
	private $password = DB_PASSWORD;
	private $database = DB_NAME;
	private $port = DB_PORT;
	private $charset = DB_CHARSET;

	private $handler;
	private $statement;
	private $error;
	private $errorCode;

	private static $db;

	public function __construct() {


		// Set DSN
		$dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->database . ';port=' . $this->port . ';charset=' . $this->charset;

		$options = [
			PDO::ATTR_PERSISTENT => true,
			PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
			PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
		];

		try {
			// Create a new PDO instance
			$this->handler = new PDO($dsn, $this->user, $this->password, $options);
		} catch(PDOException $e) {
			$this->error = $e->getMessage();
			$this->errorCode = $e->getCode();
			error_log($this->errorCode . ' ' . $this->error);
		}
	}

	/**
	 * Prepares db statements, binds parameters and executes the query
	 * @param string $sql SQL query
	 * @param array $params Array of SQL parameters to bind
	 * @param bool $fetchResults Flag to specify if the query returns results
	 * @return mixed Array of query results or the number of affected rows
	 */
	public static function query($sql, $params = null, $fetchResults = false) {

		if (!self::$db) {
			self::$db = new DB();
		}

		self::$db->prepare($sql);
		if ($params) {
			self::$db->bind($params);
		}
		self::$db->executeQuery();

		// return data for select queries 
		if (strpos(strtolower($sql), 'select') === 0) {
			$fetchResults = true;
		}

		if ($fetchResults) {
			self::$db->result = self::$db->fetchResults();
		} else {
			self::$db->result = self::$db->rowCount();
		}
		return self::$db->result;
	}

	/**
	 * Prepares a PDO statement
	 * @param string $sql SQL query string
	 */
	public function prepare($sql) {
		$this->statement = $this->handler->prepare($sql);
	}


	/**
	 * Binds parameters to the PDO statement
	 * @param array $params Array of parameters
	 */
	public function bind($params) {
		
		foreach($params as $paramKey => $paramValue) {
			if (is_int($paramValue)) {
				$type = PDO::PARAM_INT;
			} else if (is_bool($paramValue)) {
				$type = PDO::PARAM_BOOL;
			} else if (is_null($paramValue)) {
				$type = PDO::PARAM_NULL;
			} else {
				$type = PDO::PARAM_STR;
			}
			
			$this->statement->bindValue($paramKey, $paramValue, $type);
		}
	}

	/**
	 * Executes PDO statement
	 */
	public function executeQuery() {
		$this->statement->execute();
	}

	/**
	 * Fetches results of the query
	 */
	public function fetchResults() {
		return $this->statement->fetchAll();
	}

	/**
	 * Fetches first result of the query
	 */
	public function fetchOne() {
		return $this->statement->fetch();
	}

	/**
	 * Returns the row count of the query
	 * @return int
	 */
	public function rowCount() {
		return $this->statement->rowCount();
	}


	
  }