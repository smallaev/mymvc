<?php

   /**
	* Core Framework class
    * Loads the controller based on the URL parameters generted by the Router
    * 
    */
  class Core {

	protected $controller = 'DefaultController';
	protected $controllerInstance = null;
	protected $action = 'index';
	protected $params = [];

	/**
     * 
     * Loads the controller and calls the action method
     *
     * @param array $routeParams an array of URL params
	 * [
	 * 	"controller" => ControllerName,
	 * 	"action" => ActionName,
	 *  "params" => array (array of parameters)
	 * ]
     *
     */

	public function __construct($routeParams) {
		
		if (isset($routeParams['controller']) && $routeParams['controller']) {
			// capitalise the controller name
			$this->controller = ucwords($routeParams['controller']);
		}
		
		if (isset($routeParams['action']) && $routeParams['action']) {
			$this->action = $routeParams['action'];
		}
		
		if (!empty($routeParams['params'])) {
			$this->params = $routeParams['params'];
		}

		if ($this->componentControllerExists($this->controller)) {
			// if the controller file exists, require the file
			require_once __DIR__ . '/../../components/'. $this->controller . '/' . $this->controller . 'Controller.php';
		
			// instantiate the controller class
			$controllerClass = $this->controller . 'Controller'; 
			$this->controllerInstance = new $controllerClass();

			if (method_exists($this->controllerInstance, $this->action)) {
				// if the action method exists on the controller, call the method with the passed in parameters
				call_user_func_array([$this->controllerInstance, $this->action], ['parameters' => $this->params]);
			} else {
				echo "Action {$this->action} doesn't exist";
			}

		} else {
			echo "Controller {$this->controller} doesn't exist";
		}
	}

	/**
	 * Checks if the controller file exists
	 * @return bool
	 */

	public function componentControllerExists() {
		return file_exists(__DIR__ . '/../../components/'. $this->controller . '/' . $this->controller . 'Controller.php');
	}
  }
  
  