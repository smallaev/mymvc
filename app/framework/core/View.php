<?php
#
    class View {        
        
        /**
         * Loads the view
         * @param string $view - View name
         * @param array $data - Array of data passed from the controller
         * 
         */
        public static function load($view, $data = []) {
            if (strpos($view, '/') !== false) {
                // if the view name contains both the component and view names, get the view inside the component folder
                $filePathParts = explode('/', $view);
                $componentName = $filePathParts[0]; 
                $viewName = $filePathParts[1];
            } else {
                // if the view name only contains the component name, load the main view of the component 
                $componentName = $view;
                $viewName = $view;
            }

            $viewFilePath = __DIR__ . '/../../components/' . $componentName . '/views/' . $viewName;

            if (file_exists($viewFilePath . '.html')) {
                // if the custom view html template file exists, get the contents of the template and parse it
                $template = file_get_contents($viewFilePath . '.html');
                Template::parse($view, $template, $data);
            } else if (file_exists($viewFilePath . 'View.php')) {
                // if the php template view file exists, require it here directly
                require_once $viewFilePath . 'View.php';
            } else {
                echo "View $view doesn't exist";
            }
        }

        /**
         * Encodes the data as json and returns json
         * Can be used for API calls
         * @param array $data
         */
        public static function json($data = []) {
            header('Content-Type: application/json');
            $json = json_encode($data);
            echo $json;
            exit();
        }
        
    }