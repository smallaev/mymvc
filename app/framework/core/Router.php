<?php

class Router {

	/**
	 * Router class
	 * Gets the controller and action fom the URL
	 * 
	 */

    protected $_routes = [];
	
	/**
	 * Transforms routes to regex routes
	 * @param string $route - Route to be transformed to regex route
	 * @return string
	 */
	public function getRegexRoute($route) {
		$regexRoute = str_replace("/", "\/", $route);
		$regexRoute = preg_replace("/\{([\w]+)\}/", "(?P<$1>[\w]+)", $regexRoute);
        $regexRoute = '/^' . $regexRoute . '$/';
		return $regexRoute;
	}

	public function getRoutes() {
        return $this->_routes;
    }

	/**
	 * Gets controller, action and optional parameters from the URL
	 * @return array
	 */
    public function getRouteParams() {
        $method = $_SERVER['REQUEST_METHOD'];
        
		$res = [];

		if (isset($_GET['url'])) {
			$url = $_GET['url'];
			if ($url !== '/') {
				$url = trim($url, '/');
			}
			$url = filter_var($url, FILTER_SANITIZE_URL);

			// loop over all defined routes for the current request method
			foreach($this->_routes[$method] as $route => $map) {
				if ($route !== '/') {
					$route = trim($route, '/');
				}
				// transform route into regex routes
				$regexRoute = $this->getRegexRoute($route);

				// if the requested url matches one of the defined routes, return controller, action and parameters
				if (preg_match($regexRoute, $url, $matches)) {
					$res['controller'] = $map[0];
					$res['action'] = $map[1];
					$res['params'] = [];
					foreach($matches as $key => $value) {
						if (!is_int($key)) {
							$res['params'][$key] = $value;
						}
					}
					return $res;			
				}
			}
		}
    }

	/**
	 * Adds a new route to the list of routes
	 * @param string $method - Request method
	 * @param string $route
	 * @param string $controllerAction - Controller and action in the following format Controller:action
	 */
    public function add($method, $route, $controllerAction) {

		if (strpos($controllerAction, ':') !== false) {
			$controllerActionParts = explode(':', $controllerAction);
			$controller = $controllerActionParts[0]; 
			$action = $controllerActionParts[1];
		} else {
			$controller = $controllerAction; 
			$action = 'index';
		}
        $this->_routes[$method][$route] = [$controller, $action];
    }

	/**
	 * Adds a new GET route
	 */
    public function get($route, $controllerAction) {
        $this->add('GET', $route, $controllerAction);
    }

	/**
	 * Adds a new POST route
	 */
    public function post($route, $controllerAction) {
        $this->add('POST', $route, $controllerAction);
    }
}