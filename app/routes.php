<?php

$router = new Router();

// example routes
// $router->add('GET', 'user/{user_id}', 'User:getUser');
// $router->get('users/add', 'User:add');
// $router->get('projects', 'ProjectsPage:getProjects');

$router->get('/', 'HomePage:index');